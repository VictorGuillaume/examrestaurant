import os
import pandas as pd
import matplotlib.pyplot as plt

# Set up of path
outpath_data = os.path.abspath("../../Data/part1.csv")
datas = pd.read_csv(outpath_data)

# Distribution of the cost in each course
plt.figure(1)

plt.subplot(1,3,1)
x1 = datas['FIRST_COURSE']
plt.hist(x1, color='g',label='First_Course')
plt.title("Distribution cost of First_Course")
plt.xlim(0,max(datas['FIRST_COURSE']))

plt.subplot(1,3,2)
x2 = datas['SECOND_COURSE']
plt.hist(x2, color='r',label='Second_Course')
plt.title("Distribution cost of Second_Course")
plt.xlim(0,max(datas['SECOND_COURSE']))

plt.subplot(1,3,3)
x3 = datas['THIRD_COURSE']
plt.hist(x3, color='b',label='Third_Course')
plt.title("Distribution cost of Third_Course")
plt.xlim(0,max(datas['THIRD_COURSE']))

plt.show()