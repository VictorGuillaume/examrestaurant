import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

# Set up of path
outpath_data = os.path.abspath("../../Data/part1.csv")
outpath_result = os.path.abspath("../Results/part1.csv")
datas = pd.read_csv(outpath_data)

# Barplot of the cost per course
plt.figure(2)

x = []
y = ['FIRST_COURSE','SECOND_COURSE','THIRD_COURSE']
x.append(np.mean(datas['FIRST_COURSE']))
x.append(np.mean(datas['SECOND_COURSE']))
x.append(np.mean(datas['THIRD_COURSE']))

plt.bar(range(3),x,0.8,linewidth=1)
plt.xticks(range(3),y)
plt.show()