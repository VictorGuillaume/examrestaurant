import os
import pandas as pd

# Set up of path
outpath_data = os.path.abspath("../../Data/part1.csv")
outpath_result = os.path.abspath("../../Results/part1_results.csv")
datas = pd.read_csv(outpath_data)

# Add columns

def actual_food(COURSE, PRICE):
    if COURSE == 'FIRST_COURSE':
        if PRICE > 20:
            food = "Oyster"
            drink_price = PRICE - 20
        elif PRICE > 15:
            food = "Tomato-Mozarella"
            drink_price = PRICE - 15
        elif PRICE > 3:
            food = "Soup"
            drink_price = PRICE - 3
        else :
            food = ""
            drink_price = PRICE

    elif COURSE == 'SECOND_COURSE':
        if PRICE > 40:
            food = "Lobster"
            drink_price = PRICE - 40
        elif PRICE > 25:
            food = "Steak"
            drink_price = PRICE - 25
        elif PRICE > 20:
            food = "Spaghetti"
            drink_price = PRICE - 20
        elif PRICE > 9:
            food = "Salad"
            drink_price = PRICE - 9
        else:
            food = ""
            drink_price = PRICE

    else:
        if PRICE > 15:
            food = "Ice Cream"
            drink_price = PRICE - 15
        elif PRICE > 10:
            food = "Pie"
            drink_price = PRICE - 10
        else:
            food = ""
            drink_price = PRICE

    return food, drink_price


food1 = []
food2 = []
food3 = []
price1 = []
price2 = []
price3 = []

for i in range(len(datas)):
    food1.append(actual_food('FIRST_COURSE', datas.loc[i].iloc[2])[0])
    price1.append(actual_food('FIRST_COURSE', datas.loc[i].iloc[2])[1])
    food2.append(actual_food('SECOND_COURSE', datas.loc[i].iloc[3])[0])
    price2.append(actual_food('SECOND_COURSE', datas.loc[i].iloc[3])[1])
    food3.append(actual_food('THIRD_COURSE', datas.loc[i].iloc[4])[0])
    price3.append(actual_food('THIRD_COURSE', datas.loc[i].iloc[4])[1])

print(datas.head(5))

datas['STARTERS'] = food1
datas['MAINS'] = food2
datas['DESSERT'] = food3
datas['FIRST_DRINK'] = price1
datas['SECOND_DRINK'] = price2
datas['THIRD_DRINK'] = price3

datas.to_csv(outpath_result, index=False)

datas2 = pd.read_csv(outpath_result)
print(datas2.head(0))

