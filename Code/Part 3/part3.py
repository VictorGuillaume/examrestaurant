import os
import pandas as pd
import matplotlib.pyplot as plt

# Set up the dataset
outpath_data = os.path.abspath("../../Results/labels.csv")
outpath_data2 = os.path.abspath("../../Results/part1_results.csv")
outpath_result = os.path.abspath("../../Results/part3.csv")
datas_og = pd.read_csv(outpath_data)
datas = pd.read_csv(outpath_data)

# Distribution
distrib = datas.set_index(["LABELS", "CLIENT_TYPE", "TIME", "FIRST_COURSE", "SECOND_COURSE", "THIRD_COURSE"]).count(
    level="LABELS")
distrib_nb = distrib.rename(columns={'CLIENT_ID': 'NbClient'})
distrib_nb['CLIENT_TYPE'] = ['Business', 'Healthy', 'OneTime', 'Retirement']
distrib_percent = distrib_nb['NbClient'] / len(datas)

plt.pie(distrib_nb['NbClient'], labels=distrib_nb['CLIENT_TYPE'], autopct='%1.1f%%')
plt.show()


# 2 : “How likely is this type of client to get a starter, main and dessert?”


def binary(dish):
    if pd.isnull(dish):
        binar = 0
    else:
        binar = 1
    return binar


# Set up the dataset
food = pd.read_csv(outpath_data2)
distrib_food2 = pd.merge(food, datas_og, how='left', on='CLIENT_ID')
distrib_food = distrib_food2.filter(['CLIENT_ID', 'STARTERS', 'MAINS', 'DESSERT', 'LABELS'])
client_type = ['Business', 'Healthy', 'OneTime', 'Retirement']

fig = plt.figure(2)
fig.suptitle('How likely is this type of client to get a starter, main and dessert?')

# apply the binary def that give 1 when client take a dish and 0 if not. With this I can easily compute percentages
likely = pd.DataFrame.copy(distrib_food)
likely['STARTERS'] = likely['STARTERS'].map(binary)
likely['MAINS'] = likely['MAINS'].map(binary)
likely['DESSERT'] = likely['DESSERT'].map(binary)

likely_starter = []
likely_main = []
likely_dessert = []

# append the value compute to the list above depending on client type
for w in range(4):
    likely_type = likely[likely['LABELS'] == client_type[w]]
    sum1 = likely_type['STARTERS'].sum()
    sum2 = likely_type['MAINS'].sum()
    sum3 = likely_type['DESSERT'].sum()
    likely_starter.append(sum1 / len(likely_type))
    likely_main.append(sum2 / len(likely_type))
    likely_dessert.append(sum3 / len(likely_type))

# building of the percentage  of : How likely is this type of client to get a starter, main and dessert?
distrib_matrix = pd.DataFrame({'CLIENT_TYPE': client_type,
                               'STARTER': likely_starter,
                               'MAINS': likely_main,
                               'DESSERT': likely_dessert})

# 3 : Can you figure out the probability of a certain type of customer ordering a certain dish.

# Data

def count_dish_client(dish,course,client):
    x = distrib_food[distrib_food['LABELS'] == client]
    x1 = x[distrib_food[course] == dish]
    result = len(x1)/len(x)
    return result


# Building of number of dishes ordered by course and client type
percent_soup = []
percent_tomato = []
percent_oyst = []
percent_salad = []
percent_spa = []
percent_steak = []
percent_lobst = []
percent_ice = []
percent_pie = []

for l in client_type:
    percent_soup.append(count_dish_client('Soup','STARTERS',l))
    percent_tomato.append(count_dish_client('Tomato-Mozarella','STARTERS',l))
    percent_oyst.append(count_dish_client('Oyster','STARTERS',l))

for l in client_type:
    percent_salad.append(count_dish_client('Salad','MAINS',l))
    percent_spa.append(count_dish_client('Spaghetti','MAINS',l))
    percent_steak.append(count_dish_client('Steak','MAINS',l))
    percent_lobst.append(count_dish_client('Lobster','MAINS',l))

for l in client_type:
    percent_ice.append(count_dish_client('Ice cream','DESSERT',l))
    percent_pie.append(count_dish_client('Pie','DESSERT',l))

matrix_percent_dishes = pd.DataFrame({'CLIENT_TYPE': client_type,
                                      'Soup': percent_soup,
                                      'Tomato-Mozarella': percent_tomato,
                                      'Oyster': percent_oyst,
                                      'Salad': percent_salad,
                                      'Spaghetti': percent_spa,
                                      'Steak': percent_steak,
                                      'Lobster' : percent_lobst,
                                      'Ice Cream':percent_ice,
                                      'Pie': percent_pie})

# Determine the distribution of dishes, per course, per customer type.
for z in range(4):
    fig = plt.figure(z + 2)
    fig.suptitle('Distribution of dishes, per course : ' + client_type[z])
    x = distrib_food[distrib_food['LABELS'] == client_type[z]]

    ax0 = plt.subplot(z + 2, 3, 1)
    x1 = x['STARTERS']
    pd.Series(x1).value_counts().plot(kind='bar', title='STARTERS')

    ax1 = plt.subplot(z + 2, 3, 2)
    x2 = x['MAINS']
    pd.Series(x2).value_counts().plot(kind='bar', title='MAINS')
    ax1.set_title('MAINS')

    ax2 = plt.subplot(z + 2, 3, 3)
    x3 = x['DESSERT']
    pd.Series(x3).value_counts().plot(kind='bar', title='DESSERT')
    ax2.set_title('MAINS')

plt.show()

# Can you determine the distribution of the cost of the drinks per course?
datas2 = pd.read_csv(outpath_data2)

plt.figure(8)

plt.subplot(8,3,1)
drink_starter = datas2['FIRST_DRINK']
plt.hist(drink_starter, color='g',label='STARTERS DRINK', bins=10)
plt.title("Distribution cost of starters' drinks")
plt.xlim(0,max(datas2['FIRST_DRINK']))

plt.subplot(8,3,2)
drink_main = datas2['SECOND_DRINK']
plt.hist(drink_main, color='b',label='MAIN DRINK', bins=10)
plt.title("Distribution cost of mains' drinks")
plt.xlim(0,max(datas2['SECOND_DRINK']))

plt.subplot(8,3,3)
drink_dessert = datas2['THIRD_DRINK']
plt.hist(drink_dessert, color='r',label='DESSERT DRINK', bins=10)
plt.title("Distribution cost of desserts' drinks")
plt.xlim(0,max(datas2['THIRD_DRINK']))

plt.show()