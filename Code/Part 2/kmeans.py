import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from sklearn.cluster import KMeans

# Set up the dataset
outpath_data = os.path.abspath("../../Data/part1.csv")
outpath_result = os.path.abspath("../../Results/labels.csv")
datas_og = pd.read_csv(outpath_data)
datas_csv = pd.read_csv(outpath_data)
datas_part3 = pd.read_csv(os.path.abspath("../../Data/part3.csv"))

np.random.seed(5)

# Initialize datas
datas_csv.pop('CLIENT_ID')
datas_csv.pop('TIME')

datas = datas_csv.values.tolist()
datas = np.array(datas)

X = datas

name = 'k_means_course_4'
est = KMeans(n_clusters=4)
fignum = 1
titles = ['4 clusters']

# Plot the ground truth
fig = plt.figure(fignum, figsize=(4, 3))
ax = Axes3D(fig, rect=[0,0,.95,1], elev=48, azim=134)
est.fit(X)
labels = est.labels_

#Generate datas set to analyse and merge with the part3
def namelabel(label):
    if label == 0:
        name = 'OneTime'
    elif label == 1:
        name = 'Retirement'
    elif label == 2:
        name = 'Business'
    else :
        name = 'Healthy'
    return name

datas_labels = datas_og
datas_labels['LABELS'] = pd.DataFrame(labels)
datas_labels['LABELS'] = datas_labels['LABELS'].map(namelabel)
datas_labels2 = pd.merge(datas_labels,datas_part3, how='left', on='CLIENT_ID')
datas_labels2.to_csv(outpath_result, index=False)


ax.scatter(X[:, 0], X[:, 1], X[:, 2],
           c=labels.astype(np.float), edgecolor='k')
ax.w_xaxis.set_ticklabels([])
ax.w_yaxis.set_ticklabels([])
ax.w_zaxis.set_ticklabels([])
ax.set_xlabel('FIRST_COURSE')
ax.set_ylabel('SECOND_COURSE')
ax.set_zlabel('THIRD_COURSE')
ax.set_title(titles[fignum - 1])
ax.dist = 12
fignum = fignum + 1
fig.show()

for name, label in [('OneTime', 0),
                    ('Retirement', 1),
                    ('Business', 2),
                    ('Healthy', 3)]:
    ax.text3D(X[labels == label, 0].mean(),
              X[labels == label, 1].mean(),
              X[labels == label, 2].mean() , name,
              horizontalalignment='center',
              bbox=dict(alpha=.2, edgecolor='w', facecolor='w'))

# Reorder the labels to have colors matching the cluster results
y = np.choose(labels, [1, 2, 0]).astype(np.float)
ax.scatter(X[:, 0], X[:, 1], X[:, 2], c=y , edgecolor='k')
ax.w_xaxis.set_ticklabels([])
ax.w_yaxis.set_ticklabels([])
ax.w_zaxis.set_ticklabels([])
ax.set_xlabel('FIRST_COURSE')
ax.set_ylabel('SECOND_COURSE')
ax.set_zlabel('THIRD_COURSE')
ax.set_title('Ground Truth')
ax.dist = 12



fig.show()
